Connect to SiLA Servers
=======================

SiLA Server Discovery
------------------------

Use the classmetod :py:func:`SilaClient.discover <sila2.client.SilaClient.discover>` to discover SiLA Servers on the network and connect to them:

.. code-block:: python

    from sila2.client import SilaClient

    client = SilaClient.discover(...)

This also works for generated client classes:

.. code-block:: python

    from my_generated_sila2_package import Client

    client = Client.discover(...)

This method has the following signature to specify which server to connect to, and how:

.. automethod:: sila2.client.SilaClient.discover
    :noindex:

Manual connection
-----------------

If you know the address of the SiLA Server, you can instantiate a :py:class:`~sila2.client.SilaClient` directly:

.. code-block:: python

    from sila2.client import SilaClient

    client = SilaClient("127.0.0.1", 50052, root_certs=open("ca.pem", "rb").read())

.. note::

    SiLA specifies that servers using untrusted certificates must broadcast their certificate authority using the SiLA Server Discovery mechanism.
    When using manual connection, encryption information must be provided explicitly to connect to untrusted servers (e.g. using ``root_certs=...``).

This also works for generated client classes:

.. code-block:: python

    from my_generated_sila2_package import Client

    client = Client("127.0.0.1", 50052, root_certs=open("ca.pem", "rb").read())

The class initializer has the following signature to specify how to connect to the server:

.. autoclass:: sila2.client.SilaClient
    :noindex:
    :exclude-members: SiLAService, address, port, discover




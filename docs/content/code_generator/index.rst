Code Generator
==============

This SDK includes a code generation tool called ``sila2-codegen``.
It can be used to generate full SiLA Server/Client packages.

Use ``sila2-codegen --help`` to receive a list of possible options:

.. runcmd:: sila2-codegen --help

.. toctree::
    :maxdepth: 1

    structure
    new_package
    add_features
    update
    generate_feature_files

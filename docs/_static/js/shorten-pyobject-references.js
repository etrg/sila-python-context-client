// shorten types like `typing.Any` or `sila2.framework.FullyQualifiedIdentifier` to `Any` or `FullyQualifiedIdentifier`
$(document).ready(function() {
    function shorten_type_string() {
        const nameparts = this.innerHTML.split(".");
        this.innerHTML = nameparts[nameparts.length - 1];
    }

    // signature: all types with cross-links (to internal types or external via intersphinx)
    $('dl[class^="py"] a[class^="reference"][href] span[class="pre"]').each(shorten_type_string)

    // signature: all internal types, even without crosslinks (mostly for TypeVars)
    $('dl[class^="py"] span[class="pre"]').filter(function () {
        return this.innerHTML.startsWith("sila2.")
    }).each(shorten_type_string)

    // parameter types specified in docstrings: all types with cross-links (to internal types or external via intersphinx)
    $('dl[class^="py"] dl[class^="field-list"] li a[class^="reference"]').each(shorten_type_string)
})

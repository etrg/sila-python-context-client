import pytest

from sila2.framework.feature import Feature
from sila2.framework.pb2 import SiLAFramework_pb2
from tests.utils import get_feature_definition_str


@pytest.fixture(scope="session")
def basic_feature() -> Feature:
    return Feature(get_feature_definition_str("BasicDataTypes"))


@pytest.fixture(scope="session")
def silaframework_pb2_module(basic_feature) -> SiLAFramework_pb2:
    return basic_feature._pb2_module.SiLAFramework__pb2

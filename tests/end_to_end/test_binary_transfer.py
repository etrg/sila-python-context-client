import random
import secrets
import uuid
from concurrent.futures import ThreadPoolExecutor
from datetime import timedelta
from typing import List, Tuple
from uuid import UUID

import grpc
import pytest

from sila2.client.sila_client import SilaClient
from sila2.features.silaservice import SiLAServiceFeature
from sila2.framework import FullyQualifiedIdentifier
from sila2.framework.abc.binary_transfer_handler import pb2_module as binary_transfer_pb2_module
from sila2.framework.binary_transfer.binary_transfer_error import BinaryTransferError, InvalidBinaryTransferUUID
from sila2.framework.binary_transfer.client_binary_transfer_handler import ClientBinaryTransferHandler
from sila2.framework.binary_transfer.server_binary_transfer_handler import ServerBinaryTransferHandler
from sila2.framework.data_types.binary import Binary
from sila2.framework.feature import Feature
from sila2.framework.pb2 import SiLAFramework_pb2
from sila2.framework.utils import consume_generator
from sila2.server import MetadataDict
from sila2.server.feature_implementation_base import FeatureImplementationBase
from sila2.server.sila_server import SilaServer
from tests.utils import generate_port, get_feature_definition_str


class SiLAServiceImpl(SiLAServiceFeature._servicer_cls):
    feature_definitions = {
        SiLAServiceFeature.fully_qualified_identifier: SiLAServiceFeature._feature_definition,
    }

    def Get_ImplementedFeatures(self, request, context):
        return SiLAServiceFeature._unobservable_properties["ImplementedFeatures"].to_message(
            list(self.feature_definitions.keys())
        )

    def Get_ServerUUID(self, request, context):
        return SiLAServiceFeature._unobservable_properties["ServerUUID"].to_message(str(uuid.uuid4()))

    def GetFeatureDefinition(self, request, context):
        feature_id = FullyQualifiedIdentifier(request.FeatureIdentifier.value)
        feature_definition = self.feature_definitions[feature_id]
        return SiLAServiceFeature._unobservable_commands["GetFeatureDefinition"].responses.to_message(
            feature_definition
        )


@pytest.fixture()
def binary_transfer_feature():
    return Feature(get_feature_definition_str("BinaryTransfer"))


@pytest.fixture()
def metadata_feature():
    return Feature(get_feature_definition_str("Metadata"))


@pytest.fixture()
def server_client_handlers(
    binary_transfer_feature,
) -> Tuple[grpc.Server, ServerBinaryTransferHandler, ClientBinaryTransferHandler]:
    class FakeSilaServer:
        grpc_server = None
        default_binary_transfer_lifetime = timedelta(minutes=1)

    fake_server = FakeSilaServer()

    port = generate_port()
    server = grpc.server(ThreadPoolExecutor(max_workers=100))
    fake_server.grpc_server = server
    server_handler = ServerBinaryTransferHandler(fake_server)
    SiLAServiceFeature._grpc_module.add_SiLAServiceServicer_to_server(SiLAServiceImpl(), server)
    server.add_insecure_port(f"localhost:{port}")
    server.start()

    client = SilaClient("localhost", port, insecure=True)

    client_handler = ClientBinaryTransferHandler(client)

    return server, server_handler, client_handler  # must return server, else it is garbage-collected here


def test_handlers(binary_transfer_feature, server_client_handlers):
    cmd = binary_transfer_feature._unobservable_commands["TestCommand"]
    server, server_handler, client_handler = server_client_handlers

    value = secrets.token_bytes(Binary.MAX_SIZE + 1)
    param = cmd.parameters.fields[0]
    binary_field = param.data_type

    # ensure value really requires binary transfer
    with pytest.raises(ValueError, match="Binary Transfer only applies to "):
        binary_field.to_message(value)  # parameter reference is necessary for binary transfer

    # generate binary upload message
    msg = client_handler.to_message(value, toplevel_named_data_node=param)
    assert msg.__class__.__name__ == "Binary"
    assert not msg.HasField("value")
    assert msg.HasField("binaryTransferUUID")
    binary_upload_id = UUID(msg.binaryTransferUUID)

    # unpack binary upload message
    res = server_handler.to_native_type(binary_upload_id)
    assert isinstance(res, bytes)
    assert res == value

    # generate binary download message
    msg = server_handler.to_message(value)
    assert msg.__class__.__name__ == "Binary"
    assert not msg.HasField("value")
    assert msg.HasField("binaryTransferUUID")
    binary_download_id = UUID(msg.binaryTransferUUID)
    assert binary_download_id != binary_upload_id

    # unpack binary download message
    res = client_handler.to_native_type(binary_download_id)
    assert isinstance(res, bytes)
    assert res == value

    # second request should use caches value
    assert binary_download_id not in server_handler.known_binaries  # client should request deletion immediately
    assert client_handler.to_native_type(binary_download_id) == res == value

    # delete binaries from server
    assert binary_download_id not in server_handler.known_binaries
    client_handler._upload_stub.DeleteBinary(
        binary_transfer_pb2_module.DeleteBinaryRequest(binaryTransferUUID=str(binary_upload_id))
    )
    assert binary_upload_id not in server_handler.known_binaries


def test_invalid_binary_transfer_uuid(binary_transfer_feature, server_client_handlers):
    server, server_handler, client_handler = server_client_handlers

    # invalid binary upload UUID
    with pytest.raises(InvalidBinaryTransferUUID):
        server_handler.to_native_type(uuid.uuid4())

    # invalid binary deletion UUID
    request = binary_transfer_pb2_module.DeleteBinaryRequest(binaryTransferUUID=str(uuid.uuid4()))
    with pytest.raises(grpc.RpcError) as ex:
        client_handler._download_stub.DeleteBinary(request)
    assert isinstance(BinaryTransferError.from_rpc_error(ex.value), InvalidBinaryTransferUUID)

    with pytest.raises(grpc.RpcError) as ex:
        client_handler._upload_stub.DeleteBinary(request)
    assert isinstance(BinaryTransferError.from_rpc_error(ex.value), InvalidBinaryTransferUUID)

    # invalid UploadChunk UUID
    requests = (
        binary_transfer_pb2_module.UploadChunkRequest(
            binaryTransferUUID=str(uuid.uuid4()), payload=b"abc", chunkIndex=i
        )
        for i in range(3)
    )
    responses = client_handler._upload_stub.UploadChunk(requests)
    with pytest.raises(grpc.RpcError) as ex:
        consume_generator(responses)
    assert isinstance(BinaryTransferError.from_rpc_error(ex.value), InvalidBinaryTransferUUID)

    # invalid GetBinaryInfo UUID
    request = binary_transfer_pb2_module.GetBinaryInfoRequest(binaryTransferUUID=str(uuid.uuid4()))
    with pytest.raises(grpc.RpcError) as ex:
        client_handler._download_stub.GetBinaryInfo(request)
    assert isinstance(BinaryTransferError.from_rpc_error(ex.value), InvalidBinaryTransferUUID)

    # invalid GetChunk UUID
    requests = (
        binary_transfer_pb2_module.GetChunkRequest(binaryTransferUUID=str(uuid.uuid4()), offset=1, length=1)
        for _ in range(3)
    )
    with pytest.raises(grpc.RpcError) as ex:
        consume_generator(client_handler._download_stub.GetChunk(requests))
    assert isinstance(BinaryTransferError.from_rpc_error(ex.value), InvalidBinaryTransferUUID)


def test_handlers_not_connected(binary_transfer_feature):
    param = binary_transfer_feature._unobservable_commands["TestCommand"].parameters.fields[0]
    binary_field = param.data_type
    msg = SiLAFramework_pb2.Binary(binaryTransferUUID=str(uuid.uuid4()))
    with pytest.raises(NotImplementedError):
        binary_field.to_native_type(msg)
    with pytest.raises(NotImplementedError):
        binary_field.to_message(secrets.token_bytes(Binary.MAX_SIZE + 1), toplevel_named_data_node=param)


def test_from_rpc_error(binary_transfer_feature, server_client_handlers):
    # wrong status code
    err = grpc.RpcError()
    err.code = lambda: grpc.StatusCode.UNAVAILABLE
    assert not BinaryTransferError.is_binary_transfer_error(err)

    # invalid Exception type
    assert not BinaryTransferError.is_binary_transfer_error(ValueError("Random error"))
    with pytest.raises(ValueError, match="Error is no BinaryTransferError"):
        BinaryTransferError.from_rpc_error(ValueError("Random error"))

    # unparsable message
    err = grpc.RpcError()
    err.code = lambda: grpc.StatusCode.ABORTED
    err.details = lambda: b"123"
    assert not BinaryTransferError.is_binary_transfer_error(err)


def test_binary_download_failed(server_client_handlers):
    server, server_handler, client_handler = server_client_handlers

    with pytest.raises(InvalidBinaryTransferUUID):
        client_handler.to_native_type(uuid.uuid4())


def test_server_client(binary_transfer_feature):
    random_binaries = [secrets.token_bytes(Binary.MAX_SIZE + i) for i in random.sample(range(Binary.MAX_SIZE * 3), 5)]

    class BinaryTransferTestImpl(FeatureImplementationBase):
        def TestCommand(
            self,
            Param1: bytes,
            Param2: List[bytes],
            *,
            metadata: MetadataDict,
        ) -> Tuple[bytes, List[bytes]]:
            return Param1[::-1], Param2[::-1]

    class BinaryTransferTestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="BinaryTransferTest",
                server_type="TestServer",
                server_uuid=uuid.uuid4(),
                server_version="0.1",
                server_description="A server for testing binary transfer",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.set_feature_implementation(binary_transfer_feature, BinaryTransferTestImpl(self))

    port = generate_port()

    server = BinaryTransferTestServer()
    server.start_insecure("127.0.0.1", port, enable_discovery=False)
    try:
        client = SilaClient("127.0.0.1", port, insecure=True)
        resp1, resp2 = client.BinaryTransfer.TestCommand(random_binaries[0], random_binaries[1:])
        assert resp1 == random_binaries[0][::-1]
        assert resp2 == random_binaries[-1:0:-1]
    finally:
        server.stop()

from __future__ import annotations

import datetime
import time
from os.path import abspath, dirname, join
from queue import Queue
from typing import Any, Dict

from sila2.framework.command.execution_info import CommandExecutionStatus
from sila2.framework.feature import Feature
from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier
from sila2.server import ObservableCommandInstanceWithIntermediateResponses, SilaServer
from sila2.server.feature_implementation_base import FeatureImplementationBase
from tests.utils import get_feature_definition_str

root_dir = join(abspath(dirname(__file__)), "..")

TimerFeature = Feature(get_feature_definition_str("Timer"))


class TimerImpl(FeatureImplementationBase):
    _CurrentTime_producer_queue: Queue[datetime.datetime]

    def __init__(self, parent_server: SilaServer):
        super().__init__(parent_server=parent_server)
        self._CurrentTime_producer_queue = Queue()
        self.run_periodically(
            lambda: self._CurrentTime_producer_queue.put(datetime.datetime.now(tz=datetime.timezone.utc)),
            delay_seconds=1,
        )

    def Countdown(
        self,
        N: int,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any],
        instance: ObservableCommandInstanceWithIntermediateResponses[int],
    ) -> datetime.datetime:
        instance.status = CommandExecutionStatus.running
        instance.progress = 0
        for i in range(N, 0, -1):
            instance.send_intermediate_response(i)
            instance.progress = (N - i) / N
            time.sleep(1)

        return datetime.datetime.now(tz=datetime.timezone.utc)

    def CurrentTime_on_subscription(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> None:
        # optional method
        pass

import pytest

from sila2.framework.data_types.integer import Integer
from sila2.framework.feature import Feature
from sila2.framework.property.observable_property import ObservableProperty
from tests.utils import create_server_stub, get_feature_definition_str


@pytest.fixture()
def observable_property_feature():
    return Feature(get_feature_definition_str("ObservableProperty"))


def test(observable_property_feature):
    prop = observable_property_feature._observable_properties["TestProperty"]

    assert isinstance(prop, ObservableProperty)
    assert prop._identifier == "TestProperty"
    assert prop._display_name == "Test Property"
    assert prop._description == "An observable property"
    assert prop.fully_qualified_identifier == "de.unigoettingen/tests/ObservableProperty/v1/Property/TestProperty"

    assert prop.parameter_message_type.__name__ == "Subscribe_TestProperty_Parameters"
    assert prop.response_message_type.__name__ == "Subscribe_TestProperty_Responses"

    assert isinstance(prop.data_type, Integer)


def test_grpc(observable_property_feature):
    prop = observable_property_feature._observable_properties["TestProperty"]

    class ObservablePropertyImpl(observable_property_feature._servicer_cls):
        def Subscribe_TestProperty(self, request, context):
            yield prop.to_message(1)
            yield prop.to_message(2)
            yield prop.to_message(3)

    server, stub = create_server_stub(observable_property_feature._grpc_module, ObservablePropertyImpl())

    assert [prop.to_native_type(res) for res in stub.Subscribe_TestProperty(prop.get_parameters_message())] == [1, 2, 3]

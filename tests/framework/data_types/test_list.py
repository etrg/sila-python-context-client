import pytest

from sila2.framework import ValidationError
from sila2.framework.data_types.integer import Integer
from sila2.framework.feature import Feature
from tests.utils import get_feature_definition_str


@pytest.fixture()
def list_feature() -> Feature:
    return Feature(get_feature_definition_str("List"))


def test(list_feature, silaframework_pb2_module):
    list_type = list_feature._data_type_definitions["TestList"]

    msg = list_feature._pb2_module.DataType_TestList(TestList=[Integer().to_message(i) for i in range(3)])
    assert list_type.to_native_type(msg) == [0, 1, 2]

    msg = list_type.to_message([1, 2, 3])
    assert list_type.to_native_type(msg) == [1, 2, 3]


def test_constrained(list_feature):
    constrained_list = list_feature._data_type_definitions["ConstrainedList"]
    constrained_list.to_message([1, 2, 3])
    with pytest.raises(ValidationError):
        constrained_list.to_message([1, 2])
    with pytest.raises(ValidationError):
        constrained_list.to_message([1, 2, 3, 4])

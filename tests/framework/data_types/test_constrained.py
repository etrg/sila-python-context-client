import pytest

from sila2.framework.constraints.length import Length
from sila2.framework.data_types.constrained import Constrained
from sila2.framework.errors.validation_error import ValidationError
from sila2.framework.feature import Feature
from sila2.framework.pb2 import SiLAFramework_pb2
from tests.utils import get_feature_definition_str


@pytest.fixture()
def constraint_feature() -> Feature:
    return Feature(get_feature_definition_str("Constraints"))


def test_string_length(basic_feature):
    string_field = basic_feature._data_type_definitions["Str"].data_type

    t = Constrained(string_field, [Length(3)])
    msg = t.to_message("abc")
    res = t.to_native_type(msg)

    assert res == "abc"

    with pytest.raises(ValidationError):
        t.to_message("abcd")

    with pytest.raises(ValidationError):
        t.to_native_type(SiLAFramework_pb2.String(value="ab"))


def test_content_type(constraint_feature):
    request = constraint_feature._data_type_definitions["Request"]
    assert isinstance(request.data_type, Constrained)
    msg = request.to_message(b"abc")
    assert request.to_native_type(msg) == b"abc"


def test_multiple_fails_and_validation_error(constraint_feature):
    cmd = constraint_feature._unobservable_commands["TestCommand"]

    # should violate MinimalExclusive(0) and MaximalExclusive(0)
    with pytest.raises(ValidationError):
        cmd.parameters.to_message(0)

    with pytest.raises(ValidationError):
        cmd.responses.to_message(0)

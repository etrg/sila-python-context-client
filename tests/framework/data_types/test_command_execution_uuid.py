import uuid
from uuid import UUID

import pytest

from sila2.framework.command.command_execution_uuid import CommandExecutionUUID


@pytest.fixture()
def command_execution_uuid_field(silaframework_pb2_module) -> CommandExecutionUUID:
    return CommandExecutionUUID(silaframework_pb2_module)


def test(command_execution_uuid_field, silaframework_pb2_module):
    exec_id = uuid.uuid4()
    msg = command_execution_uuid_field.to_message(exec_id)
    val = command_execution_uuid_field.to_native_type(msg)

    assert isinstance(msg, silaframework_pb2_module.CommandExecutionUUID)
    assert isinstance(val, UUID)
    assert val == exec_id

import uuid
from datetime import timedelta
from math import isclose

import pytest

from sila2.framework.command.command_confirmation import CommandConfirmation


@pytest.fixture()
def command_confirmation_field(silaframework_pb2_module) -> CommandConfirmation:
    return CommandConfirmation(silaframework_pb2_module)


def test_all_set(command_confirmation_field):
    exec_id = uuid.uuid4()

    msg = command_confirmation_field.to_message(exec_id, timedelta(seconds=10.5))
    res = command_confirmation_field.to_native_type(msg)
    assert res[0] == exec_id
    assert isclose(res[1].total_seconds(), 10.5)


def test_duration_zero(command_confirmation_field):
    exec_id = uuid.uuid4()

    msg = command_confirmation_field.to_message(exec_id)
    assert command_confirmation_field.to_native_type(msg) == (exec_id, None)


def test_no_duration(command_confirmation_field):
    exec_id = uuid.uuid4()

    msg = command_confirmation_field.to_message(exec_id, timedelta(seconds=0))
    assert msg.HasField("lifetimeOfExecution")
    assert command_confirmation_field.to_native_type(msg) == (exec_id, timedelta(0))


def test_is_message(command_confirmation_field, silaframework_pb2_module):
    exec_id = uuid.uuid4()

    msg = command_confirmation_field.to_message(exec_id, timedelta(seconds=10.5))
    assert isinstance(msg, silaframework_pb2_module.CommandConfirmation)

from sila2.framework.constraints.fully_qualified_identifier import FullyQualifiedIdentifier


def test():
    con = FullyQualifiedIdentifier("FeatureIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v")
    assert not con.validate("de.unigoettingen/tests/observableProperty/v1")  # invalid identifier
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId")  # partial match

    assert repr(con) == "FullyQualifiedIdentifier('FeatureIdentifier')"

    con = FullyQualifiedIdentifier("CommandIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Property/PropertyId")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command")

    con = FullyQualifiedIdentifier("CommandParameterIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/Parameter/Param1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/Parmeter/Param1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/Parameter")

    con = FullyQualifiedIdentifier("CommandResponseIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/Response/Resp1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/Respons/Resp1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/Response")

    con = FullyQualifiedIdentifier("IntermediateCommandResponseIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/IntermediateResponse/Resp1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/IntermediateRespons/Resp1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Command/CommandId/IntermediateResponse")

    con = FullyQualifiedIdentifier("DefinedExecutionErrorIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/DefinedExecutionError/Error1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/DefinedExecutionError")

    con = FullyQualifiedIdentifier("PropertyIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/Property/PropertyId")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Propert/PropertyId")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Property")

    con = FullyQualifiedIdentifier("DataTypeIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/DataType/TypeDef")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/DataType/")

    con = FullyQualifiedIdentifier("MetadataIdentifier")
    assert con.validate("de.unigoettingen/tests/ObservableProperty/v1/Metadata/Meta1")
    assert not con.validate("de.unigoettingen/tests/ObservableProperty/v1/Metadata")

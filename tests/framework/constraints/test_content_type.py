from sila2.framework.constraints.content_type import ContentType, ContentTypeParameter


def test():
    http = ContentType(
        "application",
        "http",
        [
            ContentTypeParameter("version", "1.1"),
            ContentTypeParameter("msgtype", "request"),
        ],
    )
    assert http.media_type == "application/http; version=1.1; msgtype=request"
    assert http.validate("abc")

    assert repr(http) == "ContentType('application/http; version=1.1; msgtype=request')"

import pytest

from sila2.framework import SilaAnyType, ValidationError
from sila2.framework.feature import Feature
from sila2.framework.utils import xml_node_to_normalized_string
from tests.utils import get_feature_definition_str


@pytest.fixture()
def constraint_feature() -> Feature:
    return Feature(get_feature_definition_str("Constraints"))


def test_basic(constraint_feature):
    dtype = constraint_feature._data_type_definitions["AllowedTypes"]

    assert (
        repr(dtype.data_type.constraints[0]) == "AllowedTypes(['<DataType><Basic>Integer</Basic></DataType>', "
        "'<DataType><Constrained><DataType><Basic>String</Basic></DataType>"
        "<Constraints><Length>3</Length></Constraints></Constrained></DataType>'])"
    )

    type_str = """
    <DataType>
        <Basic>Integer</Basic>
    </DataType>"""
    val = 1232
    msg = dtype.to_message(SilaAnyType(type_str, val))
    assert dtype.to_native_type(msg) == (xml_node_to_normalized_string(type_str), val)

    type_str = """
    <DataType>
        <Basic>String</Basic>
    </DataType>"""
    val = "abc"

    with pytest.raises(ValidationError):
        _ = dtype.to_message(SilaAnyType(type_str, val))

    type_str = """
    <DataType>
        <Constrained>
            <DataType>
                <Basic>String</Basic>
            </DataType>
            <Constraints>
                <Length>3</Length>
            </Constraints>
        </Constrained>
    </DataType>"""
    val = "abc"
    msg = dtype.to_message(SilaAnyType(type_str, val))
    assert dtype.to_native_type(msg) == (xml_node_to_normalized_string(type_str), val)

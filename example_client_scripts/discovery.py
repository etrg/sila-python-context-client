from pathlib import Path

from sila2_example_server import Client


def main():
    print("Starting SiLA Discovery...")
    certificate_authority = Path("ca.pem").read_bytes()

    try:
        client = Client.discover(server_name="ExampleServer", root_certs=certificate_authority, timeout=15)

        print("Discovered SiLA Server with the following features:")
        for feature_identifier in client.SiLAService.ImplementedFeatures.get():
            print("-", feature_identifier)

    except TimeoutError:
        print("Failed to discover SiLA Server")


if __name__ == "__main__":
    main()
